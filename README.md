Home Works
====

Put your solution to the homework here.

1. Log in to your GitLab account
1. Fork this repo
1. Clone your copy of this repo
1. Create a subdir named after yourself
1. Add your own sources to the subdir
1. Commit and push to your repo
1. Make a merge request to the central repo
1. After I have approved the merge request, you can find your solution in the central repo


